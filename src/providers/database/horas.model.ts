export class HorasModel{
    public horaInicio: string;
    public horaFinal: string;
    public dia: string;
    public id: number;
    constructor( horaInicio: string, horaFinal: string, dia: string, id?: number){    
        this.horaInicio = horaInicio;
        this.horaFinal = horaFinal;
        this.dia = dia;
        if(id){
            this.id = id;
        }
    }
}