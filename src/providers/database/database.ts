import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { HorasModel } from './horas.model';


@Injectable()
export class DatabaseProvider {
  db:SQLiteObject;
  constructor(
    private sqlite: SQLite
  ) {
      sqlite = new SQLite;
      sqlite.create({
      name: 'basededatos.db',
      location: 'default'
    })
    .then((db: SQLiteObject) => {
      db.executeSql('CREATE TABLE IF NOT EXISTS Lunes(ID INTEGER PRIMARY KEY AUTOINCREMENT, INICIO TEXT, FINAL TEXT)', []);
      db.executeSql('CREATE TABLE IF NOT EXISTS Martes(ID INTEGER PRIMARY KEY AUTOINCREMENT, INICIO TEXT, FINAL TEXT)', []);
      db.executeSql('CREATE TABLE IF NOT EXISTS Miercoles(ID INTEGER PRIMARY KEY AUTOINCREMENT, INICIO TEXT, FINAL TEXT)', []);
      db.executeSql('CREATE TABLE IF NOT EXISTS Jueves(ID INTEGER PRIMARY KEY AUTOINCREMENT, INICIO TEXT, FINAL TEXT)', []);
      db.executeSql('CREATE TABLE IF NOT EXISTS Viernes(ID INTEGER PRIMARY KEY AUTOINCREMENT, INICIO TEXT, FINAL TEXT)', []);
      db.executeSql('CREATE TABLE IF NOT EXISTS Sabado(ID INTEGER PRIMARY KEY AUTOINCREMENT, INICIO TEXT, FINAL TEXT)', []);
      db.executeSql('CREATE TABLE IF NOT EXISTS Domingo(ID INTEGER PRIMARY KEY AUTOINCREMENT, INICIO TEXT, FINAL TEXT)', []);
      this.db = db;
    })
    .catch(e => alert(e));
  }
  insertarHora(hora: HorasModel){
    return new Promise((resolve, reject)=>{
        this.db.executeSql(`INSERT INTO ${hora.dia}(INICIO, FINAL) VALUES ('${hora.horaInicio}','${hora.horaFinal}')`,[]);
    }).catch(e=>alert(e));
  }
  getHoras(dia: string){
    return new Promise((resolve, reject)=>{
      this.db.executeSql(`SELECT * FROM ${dia}`,[])
      .then((data)=>{
      let horasModel = new Array<HorasModel>();
      if(data.rows.length > 0){
        for( let i = 0; i< data.rows.length; i ++){
          horasModel.push({
            dia: dia,
            id: data.rows.item(i).ID,
            horaInicio: data.rows.item(i).INICIO,
            horaFinal: data.rows.item(i).FINAL
          });
        }
      }
      resolve(horasModel)
    },(err)=>{reject(err);})
  });
  }
  
  eliminarHoras(hora: HorasModel){
    return new Promise((resolve, reject)=>{
      this.db.executeSql(`DELETE * FROM ${hora.dia} WHERE ID = ${hora.id} `,[]);
    }).catch(e=>alert(e));
  }
  
  modificarHoras(hora: HorasModel){
    return new Promise((resolve, reject)=>{
      this.db.executeSql(`UPDATE ${hora.dia} SET INICIO = '${hora.horaInicio}', FINAL = '${hora.horaFinal}' WHERE ID = ${hora.id}`,[]);
    }).catch(e=>alert(e));
  }
}
