import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DiasDetailPage } from '../pages/dias-detail/dias-detail';
import { DiasDetailToolbarPage } from '../pages/dias-detail-toolbar/dias-detail-toolbar';
import { TabsComponentPage } from '../pages/tabs-component/tabs-component';
import { UpdateHoraPage } from '../pages/update-hora/update-hora';
import { OpcionesComponentPage } from '../pages/opciones-component/opciones-component';
import { DatabaseProvider } from '../providers/database/database';
import { SQLite, SQLiteObject} from '@ionic-native/sqlite';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DiasDetailPage,
    DiasDetailToolbarPage,
    TabsComponentPage,
    UpdateHoraPage,
    OpcionesComponentPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DiasDetailPage,
    DiasDetailToolbarPage,
    TabsComponentPage,
    UpdateHoraPage,
    OpcionesComponentPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseProvider,
    SQLite,
    BluetoothSerial
  ]
})
export class AppModule {}
