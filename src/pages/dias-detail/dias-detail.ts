import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import { UpdateHoraPage } from '../update-hora/update-hora';
import { HorasModel } from '../../providers/database/horas.model';

@IonicPage()
@Component({
  selector: 'page-dias-detail',
  templateUrl: 'dias-detail.html',
})
export class DiasDetailPage {
  horas = new Array<HorasModel>();
  dia:string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    if(navParams.get('dia')){
      this.dia = navParams.get('dia');
      this.horas = navParams.get("horas");
    }
    else{
      this.horas = [];
    } 
  }
  ionViewDidLoad() {
  console.log('ionViewDidLoad DiasDetailPage');
  }
  modificarEliminar(hora: String){
    this.navCtrl.push(UpdateHoraPage,{"insertar":false});
  }
  insertar(){
    this.navCtrl.push(UpdateHoraPage,{"insertar":true, "dia":this.dia});
  }
}
