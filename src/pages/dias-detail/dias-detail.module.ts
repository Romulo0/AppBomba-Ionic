import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DiasDetailPage } from './dias-detail';

@NgModule({
  declarations: [
    DiasDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(DiasDetailPage),
  ],
})
export class DiasDetailPageModule {}
