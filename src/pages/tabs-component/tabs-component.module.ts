import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TabsComponentPage } from './tabs-component';

@NgModule({
  declarations: [
    TabsComponentPage,
  ],
  imports: [
    IonicPageModule.forChild(TabsComponentPage),
  ],
})
export class TabsComponentPageModule {}
