import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DiasDetailToolbarPage } from '../dias-detail-toolbar/dias-detail-toolbar';
import { OpcionesComponentPage } from '../opciones-component/opciones-component';

/**
 * Generated class for the TabsComponentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs-component',
  templateUrl: 'tabs-component.html',
})
export class TabsComponentPage {
  titulo: String;
  tab1 = DiasDetailToolbarPage;
  tab2 = OpcionesComponentPage;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.titulo = "Detalles de Hora"
  }
  changeTab(titulo: String){
    this.titulo = titulo;
  }
  ionViewDidLoad() {
  }

}
