import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TabsComponentPage } from '../tabs-component/tabs-component';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  variable:string;
 
  constructor(public navCtrl: NavController) {

  }
  ionViewDidLoad() {
    this.variable = "Control de Bomba";
  }
  goDiasDetail(){
    this.navCtrl.push(TabsComponentPage)
  }

}
