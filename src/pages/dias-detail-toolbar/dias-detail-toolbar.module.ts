import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DiasDetailToolbarPage } from './dias-detail-toolbar';

@NgModule({
  declarations: [
    DiasDetailToolbarPage,
  ],
  imports: [
    IonicPageModule.forChild(DiasDetailToolbarPage),
  ],
})

export class DiasDetailToolbarPageModule {
}
