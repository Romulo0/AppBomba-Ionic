import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import { DiasDetailPage } from '../dias-detail/dias-detail';
import { DatabaseProvider } from '../../providers/database/database';
import { HorasModel } from '../../providers/database/horas.model';
import { SQLite } from '@ionic-native/sqlite';

enum Dias{
  LUNES = "Lunes",
  MARTES = "Martes",
  MIERCOLES = "Miercoles",
  JUEVES = "Jueves",
  VIERNES = "Viernes",
  SABADO = "Sabado", 
  DOMINGO = "Domingo"
}

@IonicPage()
@Component({
  selector: 'page-dias-detail-toolbar',
  templateUrl: 'dias-detail-toolbar.html',
})
export class DiasDetailToolbarPage {
  @ViewChild('mycontent') navChild: NavController;
  dias= ["Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"];
  horas = new Array<HorasModel>();
  detail: any = DiasDetailPage;
  constructor(public navCtrl: NavController, private database: DatabaseProvider) {
  }
  ionViewDidLoad() {
  }
  ionViewWillEnter(){
    alert(this.horas.length +" " + this.database.db)
    if(this.database.db !== undefined && this.horas.length !== 0){
      this.database.getHoras(this.horas[0].dia).then((horas: HorasModel[]) =>{ 
        this.horas = horas;
      }).catch(err=>alert(err));
    }
  }
  
  itemSelected(dia: string){
    this.database.getHoras(dia).then((horas: HorasModel[]) =>{ 
      this.horas = horas;
      this.navChild.push(this.detail,{"horas":this.horas,"dia":dia});
      this.navChild.remove(0,1);
    }).catch(err=>alert(err));
    this.horas = [];
  }

}
