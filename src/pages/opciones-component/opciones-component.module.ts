import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OpcionesComponentPage } from './opciones-component';

@NgModule({
  declarations: [
    OpcionesComponentPage,
  ],
  imports: [
    IonicPageModule.forChild(OpcionesComponentPage),
  ],
})
export class OpcionesComponentPageModule {}
