import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, DateTime } from 'ionic-angular';
import { DatabaseProvider } from '../../providers/database/database';
import { HorasModel } from '../../providers/database/horas.model';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';

/**
 * Generated class for the UpdateHoraPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update-hora',
  templateUrl: 'update-hora.html',
})
export class UpdateHoraPage {
  accionSobreDatos: String = "Modificar";
  accionSalir: string ="Cancelar";
  insertar: Boolean = false;
  dia:string;
  horaInicio:Date;
  horaFinal:Date;
  numColumn:string = "col-4";
  constructor(public navCtrl: NavController, public navParams: NavParams, private database: DatabaseProvider, private bluetoothSerial: BluetoothSerial) {
    this.insertar = navParams.get("insertar");
    this.dia =  navParams.get("dia");
    if(this.insertar){
      this.accionSobreDatos = "Insertar";
    }
  }

  ionViewDidLoad() {
  }
  insertarModificar(){
    let horas:HorasModel = new HorasModel(this.horaInicio.toString() , 
      this.horaInicio.toString(),
      this.dia
    );
    if(this.insertar){
      this.insertarHoras(horas);
    }else{
      alert("modificar");
    }
    this.accionSalir = "Salir";
  }
  insertarHoras(horas:HorasModel){
    this.database.insertarHora(horas)
    //this.bluetoothSerial.write('hello world').then(success, failure);
  }
  finish(){
    this.navCtrl.pop();
  }

}
