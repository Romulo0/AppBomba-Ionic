import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UpdateHoraPage } from './update-hora';

@NgModule({
  declarations: [
    UpdateHoraPage,
  ],
  imports: [
    IonicPageModule.forChild(UpdateHoraPage),
  ],
})
export class UpdateHoraPageModule {}
